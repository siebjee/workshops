# Starting screen
| key | action |
|-|-|
|`screen –DR`|list of detached screen|
|`screen –r PID`|attach detached screen ses­sion|
|`screen –dmS MySes­sion`|start a detached screen ses­sion|
|`screen –r MySes­sion`|attach screen ses­sion with name MySession|

# Basics

| key | action |
|-|-|
|`^a c`| cre­ate new win­dow|
|`^a A`| set win­dow name|
|`^a w`| show all win­dow|
|`^a 1,2,3,...`| switch to win­dow n|
|`^a "`| choose win­dow|
|`^a ^a`| switch between win­dow|
|`^a d`| detach win­dow|
|`^a ?`| help|
|`^a [`| start copy, move cur­sor to the copy loca­tion, press ENTER, select the chars, press ENTER to copy the selected char­ac­ters to the buffer|
|`^a ]`| paste from buffer|

# Advanced
| key | action |
|-|-|
|`^a S`| cre­ate split screen|
|`^a TAB`| switch between split screens|
|`^a Q`| Kill all regions but the cur­rent one.|
|`^a X`| remove active win­dow from split screen|
|`^a O`| logout active win­dow (dis­able out­put)|
|`^a I`| login active win­dow (enable output)|
