#!/usr/bin/env bash

k=$(which kubectl)

$k apply -f namespaces.yaml
$k apply -f crowd.yaml
$k apply -f postgres.yaml
$k apply -f service.yaml
$k apply -f psql-service.yaml
$k port-forward service/crowd-master 8095:8095 --namespace=test
