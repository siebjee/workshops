# Modifications
| key | action |
|-|-|
|`~`|Changes the characters capital|
|`u`|Undo last action|
|`^r`|Redo the undo|
|`i`|insert mode at cursor|
|`I`|insert mode beginning of line/beginning of visual-block|
|`a`|insert mode after cursor|
|`A`|insert mode end of line|
|`o`|insert mode and new line below cursor|
|`O`|insert mode and new line above cursor|
|`y`|yank at cursor/selection|
|`Y`|yank line|
|`p`|paste after cursor|
|`P`|paste before cursor|
|`d`|delete selection|
|`dd`|delete line|
|`D`|delete till end of line|
|`C`|change till end of line|
|`x`|delete character at cursor|
|`s`|change character at cursor/selection|
|`S`|change at cursor till end of line|
|`cw`|change word at cursor|
|`cb`|change word behind cursor|
|`J`|join line below cursor to current line/selection, space separated|
|`.` (period)|repeat previous modification action|
|`<`|In visual, indent left|
|`>`|In visual, indent right|
|`<<`|indent current line left|
|`>>`|indent current line right|
|N &lt;key&gt;| repeat action N times; where N is a number|
|`:%s/search/replace/modifier`|search and replace, can also be in selection only|

# Movement
| key | action |
|-|-|
|`h`|move cursor left|
|`l`|move cursor right|
|`k`|move cursor up|
|`j`|move cursor down|
|`^`|move cursor beginning of line|
|`$`|move cursor beginning end line|
|`w`|move cursor to beginning of next word|
|`b`|move cursor to beginning of previous word|
|`gg`|move cursor to top of document|
|`G`|move cursor to bottom of document|
|`H`|move cursor to the top of the screen|
|`M`|move cursor to the middle of the screen|
|`L`|move cursor to the bottom of the screen|
|`^u`|move cursor half a page up|
|`^d`|move cursor half a page down|
|`^b`|move cursor a page up|
|`^f`|move cursor a page down|
|`{`|move cursor next block|
|`}`|move cursor previous block|
|`/`|forward search after cursor|
|`?`|backwards search before cursor|
|`n`|move to next search result, be aware which search movement you chose|
|`N`|move to previous search result, be aware which search movement you chose|
|`*`|search for next item matching word behind cursor|
|`%`|Jump to matching brace|

# Misc
| key | action |
|-|-|
|`^g`|Show current file open in the bottom|
|`:n`(ext)|Go to next file in argument list|
|`:rew`(ind)|Go to first file in argument list|
|`:q`(uit)|quit current file, add `!` to force it|
|`:w`(rite)|write current file, add `!` to force it|
|`:wa` (write-all)|write all open files, add `!` to force it|
|`:wqa` (write-quit-all)|write & quit all open files, add `!` to force it|
|`:tabe`(dit)|open new file in tab|
|`:tabf`|open existing file in current path|
|`:sp`|horizontal split|
|`:vsp`|vertical split|
|`!`|Execute shell command|
|`:`|vim console|
